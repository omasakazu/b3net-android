require 'rubygems'
require 'json'
require 'sinatra'
require './model/comment.rb'


#トップページ
get '/' do
  @comments = Comments.order_by(:posteddate.desc)
  haml :index
end

#POST時
post '/post' do
  halt "Error: username or body empty." if params[:name].empty? || params[:message].empty?
  Comments.create({
    :name => params[:name],
    :message => params[:message],
    :posteddate => Time.now
  })
  redirect '/'
end

get '/comment.json' do
  content_type :json
  comments = []
  Comments.all.each do |comment|
   comments  << {:id=>comment.id,  :name=>comment.name, :message=>comment.message}
  end
  result =  {:APIInfo=>{:version=>1.0},:Comments=>comments}
  result.to_json
end
