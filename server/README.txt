サーバ環境構築

//=====始めに=====//
Windows用に用意していますが、Ruby・Gems・SQLite3のインストール以外の手順は同じです。
Macで構築する場合はHomeBrewなどで。Developer Tool kitが云々って出るかもしれないですが
ググると簡単に出てくるはずなので調べてください。
Linuxで構築する場合もyumなりapt-getなり適当に。暇な人はソースコードからどうぞ。
MacやLinuxを使う人は自分でどうにか出来ると思うのでよろしくお願いします。

//=====概要=====//
Ruby    :メインのプログラミング言語
RubyGems:Rubyで使うパッケージのマネージャー
SQLite3 :データベース

//=====インストール（Windows）=====//
・RubyとRubyGemsをインストール
    -http://wp.graphact.com/2011/02/08/windows-ruby-gem/を参考にインストールしてください。
    ページが消えてるかもしれないので要約しておきます
        1.http://rubyforge.org/projects/rubyinstaller/からRubyInstallerをダウンロード(rubyinstaller-1.9.3-p362.exe)
        2.インストーラーを起動して指示に従いながらインストール
            :PATHは設定してもらったほうが簡単
            :コマンドラインから実行するので関連付けはせずに.rbはエディタと関連付けたほうが作業がし易い
        3.インストールは完了だが"RubyGemsで使うコンパイラがない"のでRubyGemsでインストールできないパッケージが存在する
            :もしかするとすべてのパッケージがインストールできないかもしれないので、次の作業はしたほうが無難
            :DevKitを入れなくても自分でコンパイラを指定することも可能らしい

・RubyGemsで使うコンパイラを設定
    1.http://rubyinstaller.org/downloads/からDevKitをダウンロード(DevKit-tdm-32-4.5.2-20111229-1559-sfx.exe)
    2.自己解凍ファイルになっているので、解凍した中身を、c:\RubyDevkitを作成し配置
    3.コマンドプロンプトより、配置したフォルダに移動し以下のコマンドを実行
        -------------------------------------------------------------------------
        c:\RubyDevkit>ruby dk.rb init
        c:\RubyDevkit>ruby dk.rb init
        [INFO] found RubyInstaller v1.9.3 at C:/Ruby193
        
        Initialization complete! Please review and modify the auto-generated
        'config.yml' file to ensure it contains the root directories to all
        of the installed Rubies you want enhanced by the DevKit.

        c:\RubyDevkit>ruby dk.rb install
        [INFO] Updating convenience notice gem override for 'C:/Ruby193'
        [INFO] Installing 'C:/Ruby193/lib/ruby/site_ruby/devkit.rb'
        -------------------------------------------------------------------------

・SQLite3のインストール
    なぜかしなくてもサーバが動作した。ネットを検索してると、それでもインストールしてるページが多いので念のためインストール
        1.http://sqlite.org/download.htmlからPrecompiled Binaries for Windowsの
          sqlite-dll-win32-x86-3071502.zipをダウンロード
        2.解凍し、c:\Windows\system32に配置
    シェルが欲しい場合
        3.http://sqlite.org/download.htmlからPrecompiled Binaries for Windowsの
          sqlite-shell-win32-x86-3071502.zipをダウンロード
        4.解凍し、c:\Windows\system32に配置

・その他パッケージのインストール
    1.コマンドプロンプトより、b3net/server/app (またはecho)に移動
    2.以下のコマンドを実行し、bundlerをインストールする
        --------------------------------------
        \server\echo>gem install bundler
        Successfully installed bundler-1.2.3
        1 gem installed
        Installing ri documentation for bundler-1.2.3...
        Installing RDoc documentation for bundler-1.2.3...
        --------------------------------------
    3.以下のコマンドを実行し、Gemfileに記述されたパッケージの依存関係を解決し
      インストールする(すでに入っているパッケージによって、表示結果は異なる
        --------------------------------------
        \server\echo>bundler install
        Fetching gem metadata from https://rubygems.org/...........
        Fetching gem metadata from https://rubygems.org/..
        Installing rack (1.4.4)
        Using rack-protection (1.3.2)
        Using tilt (1.3.3)
        Using sinatra (1.3.3)
        Using bundler (1.2.3)
        Your bundle is complete! Use `bundle show [gemname]` to see where a bundled gem is installed.
        --------------------------------------

・サーバの起動
    \server\echo>ruby echo-server-1.rb または \server\app>ruby server.rb で実行
    ブラウザからhttp://localhost:4567/にアクセスしてきちんと表示されれば構築完了