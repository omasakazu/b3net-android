package com.example.app;

import java.io.IOException;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import android.os.AsyncTask;

public class AsyncHttpMethod {
    String url;
    
    public AsyncHttpMethod(String url) {
        this.url = url;
    }
    
    public void Get(final HttpCallBack callBack, final List<NameValuePair> requestParams) {
        
        AsyncTask<Void, Void, Void> get = new AsyncTask<Void, Void, Void>() {
            String recieve = "";
            
            @Override
            protected Void doInBackground(Void... params) {// Backgroundで行う処理(通信)
                String query = "";
                if (requestParams != null) {
                    query = URLEncodedUtils.format(requestParams, "UTF-8");
                }
                HttpGet request = new HttpGet(url + "?" + query);
                
                DefaultHttpClient httpClient = new DefaultHttpClient();
                try {
                    recieve = httpClient.execute(request, new ResponseHandler<String>() {
                        @Override
                        public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                            // TODO Auto-generated method stub
                            switch (response.getStatusLine().getStatusCode()) {
                                case HttpStatus.SC_OK:
                                    return EntityUtils.toString(response.getEntity(), "UTF-8");
                                case HttpStatus.SC_NOT_FOUND:
                                    throw new RuntimeException("not found data");
                                default:
                                    throw new RuntimeException("error");
                            }
                        }
                        
                    });
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
                finally {
                    httpClient.getConnectionManager().shutdown();// 接続を閉じる
                }
                return null;
            }
            
            @Override
            protected void onPostExecute(Void result) {// background処理が終わった後にメインスレッドで行う処理
                if(callBack!=null) callBack.Run(recieve);
            }
        };
        get.execute();
    }
    
    public void Post(final HttpCallBack callBack, final List<NameValuePair> requestParams) {
        AsyncTask<Void, Void, Void> post = new AsyncTask<Void, Void, Void>() {
            String recieve = "";
            
            @Override
            protected Void doInBackground(Void... params) {// Backgroundで行う処理(通信)
                String query = "";
                if (requestParams != null) {
                    query = URLEncodedUtils.format(requestParams, "UTF-8");
                }
                HttpPost request = new HttpPost(url + "?" + query);
                DefaultHttpClient httpClient = new DefaultHttpClient();
                try {
                    recieve = httpClient.execute(request, new ResponseHandler<String>() {
                        @Override
                        public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                            // TODO Auto-generated method stub
                            switch (response.getStatusLine().getStatusCode()) {
                                case HttpStatus.SC_OK:
                                    return EntityUtils.toString(response.getEntity(), "UTF-8");
                                case HttpStatus.SC_NOT_FOUND:
                                    throw new RuntimeException("not found data");
                                default:
                                    throw new RuntimeException("error");
                            }
                        }
                        
                    });
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
                finally {
                    httpClient.getConnectionManager().shutdown();// 接続を閉じる
                }
                return null;
            }
            
            @Override
            protected void onPostExecute(Void result) {// background処理が終わった後にメインスレッドで行う処理
                if(callBack!=null) callBack.Run(recieve);
            }
        };
        post.execute();
    }
}
