package com.example.app;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

///
///HttpClientクラスをラッピングしたクラス
///
///GetとPostに対応
///サーバのURLを指定してインスタンス化したあとにGet/Postを呼び出して
///通信を行う
///Sample
///HttpMethod method = new HttpMethod("http://www.yahoo.co.jp");
///string msg = method.Get(null);

public class HttpMethod {
    private String Url;
    
    public HttpMethod(String Url) {
        this.Url = Url;
    }
    
    public String Get(final List<NameValuePair> requestParams) throws ClientProtocolException, IOException {
        String ret = "";
        String query = "";
        //URLにパラメータの付加
        if (requestParams != null) {
            query = URLEncodedUtils.format(requestParams, "UTF-8");
        }
        HttpGet request = new HttpGet(this.Url + "?" + query);
        
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = httpClient.execute(request);
        ret = EntityUtils.toString(response.getEntity(), "UTF-8");
        httpClient.getConnectionManager().shutdown();
        return ret;
    }
    
    public String Post(final List<NameValuePair> requestParams) throws ClientProtocolException, IOException {
        String ret = "";
        HttpPost request = new HttpPost(this.Url);
        //パラメータの付加
        request.setEntity(new UrlEncodedFormEntity(requestParams,HTTP.UTF_8));
        
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = httpClient.execute(request);
        ret = EntityUtils.toString(response.getEntity(), "UTF-8");
        httpClient.getConnectionManager().shutdown();
        return ret;
    }
}