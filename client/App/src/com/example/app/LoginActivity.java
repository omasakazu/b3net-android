package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

///
///ログイン画面クラス
///

public class LoginActivity extends Activity {
    EditText editTextName;
    EditText editTextPass;
    EditText editTextHost;
    
    TsubotterClient client; //TsubotterClientの機能を提供するインスタンス
    
    String defaultSearverUrl;
    ToastHandler toast; //ポップアップ表示のトーストラッパー
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        editTextName = (EditText) this.findViewById(R.id.editTextName);
        editTextPass = (EditText) this.findViewById(R.id.editTextPass);
        editTextHost = (EditText) this.findViewById(R.id.editTextHost);
        
        defaultSearverUrl = getString(R.string.default_servear_url);
        editTextHost.setText(defaultSearverUrl);
        
        toast = new ToastHandler(this,new Handler());
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        if (client != null) editTextName.setText(client.getName());
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_login, menu);
        return true;
    }
    
    public void LoginButtonClick(View view) {
        String name = editTextName.getText().toString();
        String pass = editTextPass.getText().toString();
        
        if (name.isEmpty() || pass.isEmpty()) {
            toast.ShowToast(R.string.no_name_pass_activity_login);
            return;
        }
        
        client = new TsubotterClient(editTextHost.getText().toString());
        if (client.Login(name, pass)) {
            MoveMain();
        }
        else {
            Toast.makeText(this, getString(R.string.fail_login_activity_login), Toast.LENGTH_LONG).show();
        }
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(getString(R.string.client_object), client);
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        client = (TsubotterClient) savedInstanceState.getSerializable(getString(R.string.client_object));
    }
    
    public void MoveMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(getString(R.string.client_object), client);
        startActivity(intent);
    }
}
