package com.example.app;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

///
///コメント送受信画面クラス
///

public class MainActivity extends Activity {
    TsubotterClient client; //TsubotterClientの機能を提供するインスタンス
    EditText editTextComment;
    ListView listViewComments;
    ListViewCommentAdapter adapter; //コメント表示用アダプター
    ArrayList<Comment> dataList = new ArrayList<Comment>();
    ToastHandler toast;//ポップアップ表示用トーストラッパー
    Handler handler;//別スレッドからコンポーネントを操作するためのハンドラー
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Intent intent = getIntent();
        client = (TsubotterClient) intent.getSerializableExtra(getString(R.string.client_object));
        
        editTextComment = (EditText) this.findViewById(R.id.EditTextComment);
        listViewComments = (ListView) this.findViewById(R.id.ListViewComments);
        
        adapter = new ListViewCommentAdapter(this, android.R.layout.simple_list_item_1, dataList);
        listViewComments.setAdapter(adapter);
        
        handler = new Handler();
        toast = new ToastHandler(this, handler);
        
        //コメントの読み込み
        new Thread(new Runnable() {
            public void run() {
                try {
                    ReloadComments();
                }
                catch (Exception e) {
                    toast.ShowToast(R.string.fail_reload_activity_main);
                    e.printStackTrace();
                    Log.v(this.toString(), e.getMessage());
                }
            }
        }).start();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        TextView textViewLoginName = (TextView) this.findViewById(R.id.TextViewLoginName);
        textViewLoginName.setText(client.getName());
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(getString(R.string.client_object), client);
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        client = (TsubotterClient) savedInstanceState.getSerializable(getString(R.string.client_object));
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void SendButtonClick(final View view) {
        final String comment = this.editTextComment.getText().toString();
        new Thread(new Runnable() {
            public void run() {
                try {// 送信
                    client.PostComment(comment);
                    toast.ShowToast(R.string.success_tweet_activity_main);
                }
                catch (Exception e) {
                    toast.ShowToast(R.string.fail_tweet_activity_main);
                    e.printStackTrace();
                    Log.v(this.toString(), e.getMessage());
                }
                try {// 受信
                    ReloadComments();
                }
                catch (Exception e) {
                    toast.ShowToast(R.string.fail_reload_activity_main);
                    e.printStackTrace();
                    Log.v(this.toString(), e.getMessage());
                }
            }
        }).start();
    }
    
    public void ReloadButtonClick(final View view) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    ReloadComments();
                }
                catch (Exception e) {
                    toast.ShowToast(R.string.fail_reload_activity_main);
                    e.printStackTrace();
                    Log.v(this.toString(), e.getMessage());
                }
            }
        }).start();
    }
    
    private void ReloadComments() throws ClientProtocolException, IOException, JSONException {
        final ArrayList<Comment> comments;
        comments = client.GetComments();
        handler.post(new Runnable() {
            public void run() {
                adapter.clear();
                for (int i = 0; i < comments.size(); i++) {
                    adapter.insert(comments.get(i), 0);
                }
            }
        });
    }
}
