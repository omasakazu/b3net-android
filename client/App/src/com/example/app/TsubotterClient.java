package com.example.app;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

///
///TsubotterClinetの機能を提供するクラス
///非同期系は廃止予定
///

public class TsubotterClient implements Serializable {
    private static final long serialVersionUID = 1L; // アクティビティ内で保存するためのシリアライズID
    
    // コメント送信時のパラメータ名
    private static final String param_message = "message";
    private static final String param_name = "name";
    
    // 接続サーバのURLとAPIのパス
    private String Url;
    private final String postPath = "/post";
    private final String commentPath = "/comment.json";
    
    private boolean logined = false;
    
    public boolean isLogined() {
        return logined;
    }
    
    // ログインユーザ名
    private String name;
    
    public String getName() {
        return name;
    }
    
    //ログインユーザパスワード
    private String pass;
    
    public String getPass() {
        return pass;
    }
    
    public TsubotterClient(String url) {
        this.Url = url;
    }
    
    public boolean Login(String name, String pass) {
        if (name.isEmpty()) {
            logined = false;
        }
        else {
            this.name = name;
            this.pass = pass;
            logined = true;
        }
        return logined;
    }
    
    //非同期コメント送信
    public void AsyncPostComment(String comment) {
        AsyncPostComment(comment, null);
    }
    
    //非同期コメント送信(コールバックあり)
    public void AsyncPostComment(String comment, HttpCallBack callBack) {
        if (!logined) return;
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(param_message, comment));
        params.add(new BasicNameValuePair(param_name, name));
        AsyncHttpMethod httpMethod = new AsyncHttpMethod(this.Url
                + this.postPath);
        httpMethod.Post(callBack, params);
    }
    
    //コメント送信
    public void PostComment(String comment) throws ClientProtocolException, IOException {
        if (!logined) return;
        //パラメタ作成
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(param_message, comment));
        params.add(new BasicNameValuePair(param_name, name));
        //サーバ接続&コメント送信
        HttpMethod httpMethod = new HttpMethod(this.Url + this.postPath);
        httpMethod.Post(params);
    }
    
    //非同期コメント取得
    public void AsyncGetComments(final ListViewCommentAdapter adapter) {
        AsyncHttpMethod httpMethod = new AsyncHttpMethod(this.Url
                + this.commentPath);
        httpMethod.Get(new HttpCallBack() {
            @Override
            public void Run(String recieveMessage) {
                try {
                    JSONObject rootObject = new JSONObject(recieveMessage);
                    JSONArray commentArray = rootObject.getJSONArray("Comments");
                    adapter.clear();
                    for (int i = 0; i < commentArray.length(); i++) {
                        JSONObject jsonObject = commentArray.getJSONObject(i);
                        int id = jsonObject.getInt("id");
                        String name = jsonObject.getString("name");
                        String message = jsonObject.getString("message");
                        Comment comment = new Comment(id, name, message);
                        adapter.insert(comment, 0);
                    }
                }
                catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.v(this.toString(), e.getMessage());
                    return;
                }
            }
        }, null);
    }
    
    //コメント取得
    public ArrayList<Comment> GetComments() throws ClientProtocolException, IOException, JSONException {
        ArrayList<Comment> comments = new ArrayList<Comment>(); //取得したコメント記憶
        
        //リクエスト送信
        HttpMethod httpMethod = new HttpMethod(this.Url + this.commentPath);
        String recieveMessage = httpMethod.Get(null);
        
        //取得したコメント(JSON)の解析
        JSONObject rootObject = new JSONObject(recieveMessage);
        JSONArray commentArray = rootObject.getJSONArray("Comments");
        for (int i = 0; i < commentArray.length(); i++) {
            JSONObject jsonObject = commentArray.getJSONObject(i);
            int id = jsonObject.getInt("id");
            String name = jsonObject.getString("name");
            String message = jsonObject.getString("message");
            comments.add(new Comment(id, name, message));
        }
        
        return comments;
    }
}
