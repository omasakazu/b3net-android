package com.example.app;

public class Comment {
    private int id;
    private String name;
    private String message;
    
    public int getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getMessage() {
        return message;
    }
    
    public Comment(int id, String name, String message) {
        this.id = id;
        this.name = name;
        this.message = message;
    }
}
