package com.example.app;

///
///Httpで通信が終了したときに呼ばれるコールバック用インターフェース
///

public interface HttpCallBack {
    public void Run(String recieve);
}
