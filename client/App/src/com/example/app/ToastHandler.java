package com.example.app;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

///
///ポップアップ表示用のトーストラッパー
///res/values/strings.xmlに記述された文字列を表示する
///

public class ToastHandler {
    final Context context;
    final Handler handler;
    
    public ToastHandler(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }
    
    public void ShowToast(final int stringId) {
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(context, context.getString(stringId), Toast.LENGTH_LONG).show();
            }
        });
    }
}
