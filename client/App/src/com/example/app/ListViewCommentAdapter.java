package com.example.app;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

///
///コメント表示用のListViewとコメントデータリストのアダプタークラス
///コメントデータの追加・削除はこのクラスから行う
///

public class ListViewCommentAdapter extends ArrayAdapter<Comment>{
    private LayoutInflater layoutInflater;
    
    public ListViewCommentAdapter(Context context ,int textViewResourceId,List<Comment> objects){
        super(context,textViewResourceId,objects);
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        //保存用リストのposition番目のコメントをitemに記憶
        Comment item = (Comment)getItem(position);
        if(convertView==null){//レイアウトが空なら再読み込み
            convertView = layoutInflater.inflate(R.layout.comment, null);
        }
        //表示用アイテムの整形
        TextView name = (TextView)convertView.findViewById(R.id.TextViewName);
        TextView message = (TextView)convertView.findViewById(R.id.TextViewMessage);
        name.setText(item.getName());
        message.setText(item.getMessage());
        return convertView;
    }
}
