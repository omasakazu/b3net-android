package com.example.echo;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import com.loopj.android.http.AsyncHttpClient;

public class EchoClient {
	private String Url = "";
	public String getUrl() {
		return Url;
	}
	public void setUrl(String url) {
		Url = url;
	}
	String ParamName = "message";

	private AsyncHttpClient client = new AsyncHttpClient();

	public EchoClient(String url) {
		Url = url;
	}
	
	public void Push(final String msg, final ArrayAdapter<String> adapter) {
		final String requestURL = this.CreateRequestUrl(msg);
		// 非同期処理
		AsyncTask<Void, Void, Void> push = new AsyncTask<Void, Void, Void>() {
			String recieve = "";

			@Override
			protected Void doInBackground(Void... params) {// Backgroundで行う処理(通信)
				HttpGet request = new HttpGet(requestURL);
				DefaultHttpClient httpClient = new DefaultHttpClient();
				try {
					recieve = httpClient.execute(request,
							new ResponseHandler<String>() {
								@Override
								public String handleResponse(
										HttpResponse response)
										throws ClientProtocolException,
										IOException {
									// TODO Auto-generated method stub
									switch (response.getStatusLine()
											.getStatusCode()) {
									case HttpStatus.SC_OK:
										return EntityUtils.toString(
												response.getEntity(), "UTF-8");
									case HttpStatus.SC_NOT_FOUND:
										throw new RuntimeException(
												"not found data");
									default:
										throw new RuntimeException("error");
									}
								}

							});
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					httpClient.getConnectionManager().shutdown();// 接続を閉じる
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {// background処理が終わった後にメインスレッドで行う処理
				recieve = recieve.substring((ParamName + ":").length(),
						recieve.length() - "<br>".length());// 整形
				adapter.insert(recieve,0);
			}
		};
		push.execute();
	}
	private String CreateRequestUrl(String msg){
		return Url + "?" + ParamName + "=" + msg;
	}
}
