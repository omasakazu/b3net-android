package com.example.echo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity {
	//文字列定数
	static String DefaultServerUrl;
	static String AlertDialogTitle;
	static String AlertDialogMessage;
	static String AlertDialogPositiveButtonText;
	static String AlertDialogNegativeButtonText;
	// UIコントロール
	ListView ListView_RecieveMessage;
	EditText editText_SendMessage;
	EditText editText_Url;

	// アラートダイアログ
	AlertDialog changeUrlAlertDialog;

	// フィールド
	List<String> dataList = new ArrayList<String>(); // log記憶
	ArrayAdapter<String> adapter; // ListViewのアダプター
	EchoClient client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//文字列定数読み込み
		DefaultServerUrl = getString(R.string.DefaultServerUrl);
		AlertDialogTitle = getString(R.string.AlertDialogTitle);
		AlertDialogMessage = getString(R.string.AlertDialogMessage);
		AlertDialogPositiveButtonText = getString(R.string.AlertDialogPositiveButtonText);
		AlertDialogNegativeButtonText = getString(R.string.AlertDialogNagativeButtonText);
		
		client = new EchoClient(DefaultServerUrl);
		
		// UIコントロールを取得
		ListView_RecieveMessage = (ListView) findViewById(R.id.ListView_ReieveMessage);
		editText_SendMessage = (EditText) findViewById(R.id.editText_SendMessage);
		editText_Url = (EditText) findViewById(R.id.editText_Url);
		// ListViewにアダプターをセット
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, dataList);
		ListView_RecieveMessage.setAdapter(adapter);

		// アラートダイアログを作成
		AlertDialog.Builder changeUrlAlertDialogBuilder = new AlertDialog.Builder(
				this);
		changeUrlAlertDialogBuilder.setTitle(AlertDialogTitle);
		changeUrlAlertDialogBuilder.setMessage(AlertDialogMessage);
		changeUrlAlertDialogBuilder.setIcon(R.drawable.ic_launcher);
		changeUrlAlertDialogBuilder.setPositiveButton(AlertDialogPositiveButtonText,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						client.setUrl(editText_Url.getText().toString());
						button_Send_Click(null);
					}
				});
		changeUrlAlertDialogBuilder.setNegativeButton(AlertDialogNegativeButtonText,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						editText_Url.setText(client.getUrl());
						button_Send_Click(null);
					}
				});
		changeUrlAlertDialog = changeUrlAlertDialogBuilder.create();
		// デフォルトURLをUrlエディットテキストに表示
		editText_Url.setText(DefaultServerUrl);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	// sendボタンをクリックした時の処理
	public void button_Send_Click(View view) {
		//Urlに変更が合った場合はアラーとダイアログがブロッキング処理してくれないため
		//アラートダイアログで確認し、整合をとった後再起的に呼び出してプッシュする
		if (!editText_Url.getText().toString().equals(client.getUrl())) {
			changeUrlAlertDialog.show();
		} else {
			client.Push(editText_SendMessage.getText().toString(), adapter);
		}
	}

}
